package dawson;

import static org.junit.Assert.*;

import org.junit.Test;

/**
 * Unit test for simple App.
 */
public class AppTest 
{
    /**
     * Rigorous Test :-)
     */
    @Test
    public void echoShouldReturn5()
    {
        assertEquals("testing echo method with input 5", 5, App.echo(5));
    }

    @Test
    public void oneMoreTesting()
    {
        assertEquals("testing oneMore method with input 5, should return 6", 6, App.echo(5));
    }
}
